# 结构化数据表示方法及vscode查看插件

定义对结构化数据进行描述的schema，并使用vscode插件进行查看

项目来源: [开源软件供应链亮点计划](https://summer.iscas.ac.cn/#/org/prodetail/210180823)

## 目录

- [仓库说明](README.md#repository)

- [Shema定义](README.md#schema-definition)

- [参考](README.md#reference)

## Repository

### dwarft

一些c语言代码和格式规范

### [jsonViewer-html](jsonViewer-html/README.md)

json格式化显示的网页demo

### [structView-Vue](structView-Vue/README.md)

将json格式的结构体数据可视化的Vue页面

### [struct2json-extension](struct2json-extension/README.md)

c语言可视化显示的vscode插件，集成了上面两个页面

## Schema Definition
### 基本定义

[JSON Schema](https://json-schema.org/)是一种用来描述和检验JSON数据的格式，规定了数据类型，用于验证json数据的正确性。c/c++的struct是用户定义的一组不同数据类型的合集。

Json的数据类型有

* 字符串
* 数字
* 对象（JSON 对象）
* 数组
* 布尔
* Null

C语言的数据类型有

* 整型
* 浮点型
* 枚举类型（暂不考虑）
* void类型（不能定义变量）
* 指针类型
* 数组类型
* 结构类型
* 联合类型（不考虑）
* 函数类型（不考虑）

**枚举类型、函数类型和联合类型暂不考虑，二者对应关系如下**

| C语言类型           | JSON   |
| ------------------- | ------ |
| 整型、浮点型        | 数字   |
| char、char[]、char* | 字符串 |
| 数组类型            | 数组   |
| 指针类型            | Null   |
| 结构类型            | 对象   |

**具体数据结构的实现**

| C语言类型 | JSON Schema                                                               |
| --------- | ------------------------------------------------------------------------- |
| short     | "type":"integer","maximum":32767,"minimun":-32768                         |
| int/long  | "type":"integer","maximum":2147483647,"minimun":-2147483648               |
| float     | "type":"string","pattern":"^([-+]?[0-9]*\\.?[0-9]+)$\|([0-9]+([eE][0-9]+))" |
| double    | "type":"string","pattern":"^([-+]?[0-9]*\\.?[0-9]+)$\|([0-9]+([eE][0-9]+))" |
| char      | "type":"string","maxLength":1,"minLength":1                               |
| char*     | "type":"string"                                                           |
| char [N]  | "type":"string","maxLength":N                                             |

### 实现方案

1. 通过结构体的定义语句生产JSON Schema
2. 根据结构体初始化语句，结合关键词生成JSON
3. 保存到本地，完成持久化
4. vscode可视化

### 举例

#### 结构体

```c
struct apple
{
    int num;
    char*  color;
    float weight;
    char class;
    short status;
    char origin[20];
};
```

#### 对应生成的JSON Schema

```json
{
    "type":"object",
    "description":"A c struct",
    "title":"apple",
    "required":[
        "num",
        "color",
        "weight",
        "class",
        "status",
        "origin"
    ],
    "properties":{
        "num":{
            "type":"integer",
            "maximum":2147483647,
            "minimun":-2147483648
        },
        "color":{
            "type":"string"
        },
        "weight":{
            "type":"string",
            "pattern":"^([-+]?[0-9]*\\.?[0-9]+)$|([0-9]+([eE][0-9]+))"
        },
        "class":{
            "type":"string",
            "maxLength":1,
            "minLength":1
        },
        "status":{
            "type":"integer",
            "maximum":32767,
            "minimun":-32768
        },
        "origin":{
            "type":"string",
            "maxLength":20
        }
    }
}
```

#### 结构体初始化

```c
const struct apple apple3 = {10,"red",0.8,'A',1,"China"};
```

#### 对应生成的JSON

```json
{
    "num":10,
    "color":"red",
    "weight":"0.8",
    "class":"A",
    "status":1,
    "origin":"China"
}
```

## Reference

[JSON折叠显示库](https://github.com/abodelot/jquery.json-viewer)

[JSON格式化显示](https://github.com/BigHeisenberg/JSONFormat)

[JSON Schema验证规范](https://json-schema.org/draft/2020-12/json-schema-validation.html)

[JSON Schema在线验证](http://json-schema-validator.herokuapp.com/)


**其它**

[VSCode插件开发全攻略](https://www.cnblogs.com/liuxianan/p/vscode-plugin-overview.html)

[使用vue开发vscode插件](https://www.cnblogs.com/oscar1987121/p/13048382.html)
