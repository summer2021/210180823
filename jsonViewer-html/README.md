# jsonViewer

json格式化显示的网页demo

## 使用说明

1. 选择文件
   打开readFile.html，点击“选择文件”按钮，选择需要生成schema的.c/.h文件。
2. 生成Schema
   下方的输入框里将生成对应的Json schema，可以对其进行修改。
3. Schema的折叠展示
   点击下方的“折叠展示”按钮，进行优化展示，对于过长的嵌套可以点击小三角进行折叠。
4. Schema验证
   在[JSON在线解析](https://www.json.cn/json/jsononline.html#)不勾选“保留转义”，将解析结果复制到[JSON Schema验证](http://json-schema-validator.herokuapp.com/)的Schema中，即可对JSON进行验证。