//字典
type_dict={
    short:  '                "type":"integer",\n                "maximum":32767,\n                "minimun":-32768',
    int:    '                "type":"integer",\n                "maximum":2147483647,\n                "minimun":-2147483648',
    long:   '                "type":"integer",\n                "maximum":2147483647,\n                "minimun":-2147483648',
    float:  '                "type":"string",\n                "pattern":"^([-+]?[0-9]*\\\\\\\\.?[0-9]+)$\|([0-9]+([eE][0-9]+))"',
    double: '                "type":"string",\n                "pattern":"^([-+]?[0-9]*\\\\\\\\.?[0-9]+)$\|([0-9]+([eE][0-9]+))"',
    char:   '                "type":"string",\n                "maxLength":1,\n                "minLength":1',
    char_p: '                "type":"string"',
    char_a: '                "type":"string",\n                "maxLength":'
}


var schema = ''

const schema_head = '\
{\n\
    "$schema": "http:\\/\\/json-schema.org/draft-07/schema",\n\
    "type": "object",\n\
    "description": "A c struct",\n\
    "title": "struct schema",\n\
'

const schema_required_start = '\
    "required": [\n\
'
const schema_required_end = '\n    ],\n';
const schema_properties_start = '\
    "properties": {\n\
'
const schema_properties_end = '    }\n';
const schema_end = '}';

var schema_required='';
var schema_properties='';
var property_required = '';
var property_properties = '';
//字符串拼接Schema
function schemaJoint(struct_list){
    console.log("struct_list："+struct_list);
    schema_required = schema_required_start;
    schema_properties = schema_properties_start;
    for(let i=0;i<struct_list.length;i++){
        if(i!=0){
            schema_required+=",\n";
            schema_properties+=",\n";
        }
        schema_required+='        "'+getStructName(struct_list[i])+'"';
        schema_properties+=getProperty(struct_list[i]);
    }
    
    schema = schema_head+schema_required+schema_required_end+schema_properties+schema_properties_end+schema_end;
    // console.log(schema);
    return schema;
}

//从struct字符串中提取结构体名
function getStructName(str){
    return str.match(/struct (.*)/)[1];
}

//从每个struct字符串中提取property
function getProperty(str){
    let title = getStructName(str);
    let property_head = '    "'+title+'":{\n\
        "description":"'+title+' struct",\n\
        "type":"array",\n\
        "item":{\n\
            "type":"object",\n\
        '
    let property_end = '    }\n}\n';
    property_required = schema_required_start;
    property_properties = '    '+schema_properties_start;
    property_required_end = schema_required_end;
    property_properties_end = '\n    '+schema_properties_end;
    let property_list=str.replace(/(.|\n)*{|};|\n/g,"").replace(/struct .*/,"").split(';');
    for(var i=0;i<property_list.length-1;i++){
        proportyDeal(property_list[i],i>=property_list.length-2);//判断是否为最后一个元素
    }
    let property = property_head+property_required+property_required_end+property_properties+property_properties_end+property_end;
    console.log("!"+property);
    return property;
}

//逐句处理属性语句
function proportyDeal(str,last=false){
    let is_point = false;//指针
    let is_array = false;//数组
    let array_length = 0;//数组长度
    if(str.indexOf("*")!=-1){
        is_point = true;
        str=str.replace(/\*/," ");
    }
    // 去掉多余的空格和回车
    str = str.replace(/(^ +|\s *)+/,"").replace(/ +/," ");
    let var_list = str.split(" ");
    let var_type = var_list[0];
    let var_name = var_list[1];
    if(var_name.search(/\[\d*\]/)!=-1){
        is_array=true;
        array_length = var_name.slice(var_name.search(/\[\d*\]/)+1,-1);
        var_name = var_name.replace(/\[\d*\]/,"");
    }
    // console.log(var_type);

    //添加到required和properties
    addRequired(var_name,last);
    addProperties(var_name,var_type,is_point,is_array,array_length,last);
}

// 拼接required属性
function addRequired(required_keyword,last=false){
    property_required +=  jsonString(required_keyword,16,last);
}

// 拼接properties属性
function addProperties(properties_name,properties_type,point_flag,array_flag,array_length,last=false){
    if(point_flag==true)properties_type+='_p';
    if(array_flag==true)properties_type+='_a';
    let description = jsonString('description": "properties of struct variable',16);

    let type = type_dict[properties_type];
    // 如果数组长度大于0
    if(array_length>0)type+=String(array_length)
    var properties_end = last?'\n            }':'\n            },\n';
    property_properties += jsonObject(properties_name,12)+description+type+properties_end;
}

//JSON字符串
function jsonString(str,tabNum=0,last=false){
    let start = '"'
    let end = '",\n'
    for(let i=0;i<tabNum;i++)start=' '+start;
    if(last)end='"\n'
    return start+str+end;
}
//JSON对象
function jsonObject(str,tabNum=0){
    var start = '"'
    for(let i=0;i<tabNum;i++)start=' '+start;
    return start+str+'": {\n';
}
