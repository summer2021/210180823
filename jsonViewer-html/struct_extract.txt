
struct apple
{
    int num;
    char*  color;
    float weight;
    char class;
    short status;
    char origin[20];
};

struct led led1 = {"red",  LED_ON};

struct led led2 = {"blue", LED_OFF};

struct apple apple1 = {10,"red",0.8,'A',1,"China"};

struct apple apple2 = {10,"blue",1.35,'C',0,"American"};
