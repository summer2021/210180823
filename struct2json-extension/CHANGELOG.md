# Change Log

## 1.0.0
- initial extension

## 1.0.1
- `added` struct to jsonschema
- `added` jsonschema format

## 1.0.2
- `added` character counter

## 1.0.3
- `fixed` bugs

## 1.0.4
- `fixed` remove unnecessary code

## 1.0.5
- `fixed` incorrect display issue

## 1.1.0
- `added` struct data display

## 1.1.1
- `added` support variable type `struct` and `pointer`

## [Unreleased]

- exception catch
- display optimization
- binary file edit