# struct2jsonschema

以JsonSchema格式定义并可视化显示C语言的结构体

## 使用说明

在vscode插件或在[插件网页](https://marketplace.visualstudio.com/vscode)上搜索`struct2json`，并安装启用

在.c文件右键，选择`view code in json`或`view struct data`


## 特性

### 1 结构体生成jsonSchema

c语言结构体

![feature 1](img/demo1-1.png)

插件可视化schema

![feature 1](img/demo1-2.png)

### 2 结构体可视化

![feature 2](img/demo2.png)

### 3 字符统计

![feature 3](img/demo3.png)

## 版本信息

### 1.0.0

初始化插件

### 1.0.1

增加特性 结构体可视化

### 1.0.2

增加特性 字符统计

### 1.1.0

增加特性 结构体的数据分块显示页面

### 1.1.1

增加特性 排除结构体嵌套和指针类型的报错

### 1.1.2

修改 更换图标


## 感谢支持

* [开源软件供应链点亮计划](https://summer.iscas.ac.cn/#/org/prodetail/210180823)

