//简化的json格式
const jsonFactory = {
    //字典
    type_dict: {
        integer:'"type":"integer"',
        number:'"type":"number"',
        // short: '"type":"integer"',
        // int: '"type":"integer"',
        // long: '"type":"integer"',
        // float: '"type":"string"',
        // double: '"type":"string"',
        char: '"type":"string",\n"maxLength":1',
        char_p: '"type":"string"',
        char_a: '"type":"string",\n"maxLength":',
        struct: '"type":"object"',
        array: '"type":"array",\n"maxItems":',
        pointer:'',
        other: '"type":"string"'
    },
    schema: '',
    schema_head: '{\n"type": "object",\n',
    schema_required_start: '"required": [',
    schema_required_end: '],\n',
    schema_properties_start: '"properties": {\n',
    schema_properties_end: '}\n',
    schema_end: '}',

    property_required: '',
    property_properties: '',
    //字符串拼接Schema
    schemaJoint(struct_list) {
        let schema_required = this.schema_required_start;
        let schema_properties = this.schema_properties_start;
        for (let i = 0; i < struct_list.length; i++) {
            if (i != 0) {
                schema_required += ",";
                schema_properties += ",\n";
            }
            schema_required += '"' + this.getStructName(struct_list[i]) + '"';
            schema_properties += this.getProperty(struct_list[i]);
        }
        let schema = this.schema_head + schema_required + this.schema_required_end + schema_properties + this.schema_properties_end + this.schema_end;
        // console.log("schema："+schema);
        return schema;
    },

    /**
     *从struct字符串中提取结构体名
    */
    getStructName(str) {
        return str.match(/struct (.*)/)[1];
    },

    /**
     * 从每个struct字符串中提取结构体的变量
     */
    getProperty(str) {
        let title = this.getStructName(str);
        let property_head = '"' + title + '":{\n"type":"array",\n"item":{\n"type":"object",\n'
        let property_end = '}\n}\n';
        this.property_required = this.schema_required_start;
        this.property_properties = this.schema_properties_start;
        let property_required_end = this.schema_required_end;
        let property_properties_end = this.schema_properties_end;
        let property_str = str.replace(/(.|\n)*{|};|\n/g, "");
        let property_list = property_str.split(';');//变量定义语句列表
        for (let i = 0; i < property_list.length - 1; i++) {
            this.proportyDeal(property_list[i], i >= property_list.length - 2);
        }
        let property = property_head + this.property_required + property_required_end + this.property_properties + property_properties_end + property_end;
        return property;
    },

    /**
     * 逐句处理属性定义语句
     * @param {string} str 结构体内变量定义语句字符串，不含分号
     * @param {boolean} last 是否为最后一个变量
     */
    proportyDeal(str, last = false) {
        let is_point = false;//指针
        let is_array = false;//数组
        let array_length;//数组长度
        //判断是否为指针类型
        if (str.indexOf("*") != -1) {
            is_point = true;
            str = str.replace(/\*/, " ");
        }
        //去掉多余的空格和回车
        str = str.replace(/(^ +|\s *)+/, "").replace(/ +/, " ");
        let var_list = str.split(" ");
        let var_type = var_list[0];
        let var_name = var_list[var_list.length-1];
        //数组类型处理
        if (var_name.search(/\[\d*\]/) != -1) {
            is_array = true;
            array_length = var_name.slice(var_name.search(/\[\d*\]/) + 1, -1);
            var_name = var_name.replace(/\[\d*\]/, "");
        }

        //添加到required和properties
        this.addRequired(var_name, last);
        this.addProperties(var_name, var_type, is_point, is_array, array_length, last);
    },

    //拼接required属性
    addRequired(required_keyword, last = false) {
        this.property_required += this.jsonString(required_keyword, last);
    },

    //拼接properties属性
    addProperties(properties_name, properties_type, point_flag, array_flag, array_length, last = false) {
        
        // if (point_flag == true) properties_type += '_p';
        // if (array_flag == true) properties_type += '_a';
        let type = 'other';
        //判断变量类型并归为不同分类
        if(properties_type.indexOf("char") != -1){
            type = 'char'
            if (point_flag == true) type += '_p';
            if (array_flag == true) type += '_a';
        }else if(point_flag == true){
            type = 'pointer';
        }else if(array_flag == true){
            type = 'array';
        }else if(properties_type.indexOf("int") != -1 || properties_type.indexOf("short") != -1 ){
            type = 'integer';
        }else if(properties_type.indexOf("long") != -1 || properties_type.indexOf("float") != -1 || 
        properties_type.indexOf("double") != -1){
            type = 'number';
        }else if(properties_type == 'struct'){
            type = 'struct';
        }

        let type_content = this.type_dict[type];
        //如果数组长度大于0
        if (array_length > 0) type_content += String(array_length)
        let properties_end = last ? '\n}\n' : '\n},\n';
        this.property_properties += this.jsonObject(properties_name) + type_content + properties_end;
        // if(properties_name=='AA'){
        //     console.log('AA:', properties_type, type, type_content, typeof(properties_type))
        // }
    },

    //JSON字符串
    jsonString(str, last = false) {
        return last?'"' + str + '"':'"' + str + '",';
    },

    //JSON对象
    jsonObject(str) {
        return '"' + str + '": {\n';
    }
}

module.exports = jsonFactory