# structView

将json格式的结构体数据可视化的Vue页面

> A Vue.js project

## 使用说明

``` bash
# 安装依赖
npm install

# 在localhost:8080运行
npm run dev

# 打包成静态文件
npm run build

```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## 预览

![](img/struct-view.png)
