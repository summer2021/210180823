/*!
 * jQuery Json Plugin (with Transition Definitions)
 * Examples and documentation at: http://json.cn/
 * Copyright (c) 2012-2013  China.Ren.
 * Version: 1.0.2 (19-OCT-2013)
 * Dual licensed under the MIT and GPL licenses.
 * http://jquery.malsup.com/license.html
 * Requires: jQuery v1.3.1 or later
 */
let JSONFormat = (function () {
  let _toString = Object.prototype.toString

  function format (object, indentCount) {
    let htmlFragment = ''
    switch (_typeof(object)) {
      case 'Null' :
        htmlFragment = _format_null(object)
        break
      case 'Boolean' :
        htmlFragment = _format_boolean(object)
        break
      case 'Number' :
        htmlFragment = _format_number(object)
        break
      case 'String' :
        htmlFragment = _format_string(object)
        break
      case 'Array' :
        htmlFragment = _format_array(object, indentCount)
        break
      case 'Object' :
        htmlFragment = _format_object(object, indentCount)
        break
    }
    return htmlFragment
  }

  // eslint-disable-next-line camelcase
  function _format_null (object) {
    return '<span class="json_null">null</span>'
  }

  // eslint-disable-next-line camelcase
  function _format_boolean (object) {
    return '<span class="json_boolean">' + object + '</span>'
  }

  // eslint-disable-next-line camelcase
  function _format_number (object) {
    return '<span class="json_number">' + object + '</span>'
  }

  // eslint-disable-next-line camelcase
  function _format_string (object) {
    object = object.replace(/</g, '&lt;')
    object = object.replace(/>/g, '&gt;')
    if (object.search(/^http/) >= 0) {
      object = '<a href="' + object + '" target="_blank" class="json_link">' + object + '</a>'
    }
    return '<span class="json_string">"' + object + '"</span>'
  }

  // eslint-disable-next-line camelcase
  function _format_array (object, indentCount) {
    let tmpArray = []
    for (let i = 0, size = object.length; i < size; ++i) {
      tmpArray.push(indentTab(indentCount) + format(object[i], indentCount + 1))
    }
    return '<span data-type="array" data-size="' + tmpArray.length + '"><i  style="cursor:pointer;" class="fa fa-minus-square-o" onclick="hide(this)"></i>[<br/>' +
      tmpArray.join(',<br/>') + '<br/>' + indentTab(indentCount - 1) + ']</span>'
  }

  // eslint-disable-next-line camelcase
  function _format_object (object, indentCount) {
    let tmpArray = []
    for (let key in object) {
      tmpArray.push(indentTab(indentCount) + '<span class="json_key">"' + key + '"</span>:' + format(object[key], indentCount + 1))
    }
    return '<span  data-type="object"><i  style="cursor:pointer;" class="fa fa-minus-square-o" onclick="hide(this)"></i>{<br/>' +
      tmpArray.join(',<br/>') + '<br/>' + indentTab(indentCount - 1) + '}</span>'
  }

  function indentTab (indentCount) {
    return (new Array(indentCount + 1)).join('&nbsp;&nbsp;&nbsp;&nbsp;')
  }

  function _typeof (object) {
    let tf = typeof object
    let ts = _toString.call(object)
    return object === null ? 'Null'
      : tf === 'undefined' ? 'Undefined'
        : tf === 'boolean' ? 'Boolean'
          : tf === 'number' ? 'Number'
            : tf === 'string' ? 'String'
              : ts === '[object Function]' ? 'Function'
                : ts === '[object Array]' ? 'Array'
                  : ts === '[object Date]' ? 'Date' : 'Object'
  }

  function loadCssString () {
    let style = document.createElement('style')
    style.type = 'text/css'
    let code = Array.prototype.slice.apply(arguments).join('')
    try {
      style.appendChild(document.createTextNode(code))
    } catch (ex) {
      style.styleSheet.cssText = code
    }
    document.getElementsByTagName('head')[0].appendChild(style)
  }

  loadCssString(
    '.json_key{ color: #92278f;font-weight:bold;}',
    '.json_null{color: #f1592a;font-weight:bold;}',
    '.json_string{ color: #3ab54a;font-weight:bold;}',
    '.json_number{ color: #25aae2;font-weight:bold;}',
    '.json_link{ color: #717171;font-weight:bold;}',
    '.json_array_brackets{}')

  let _JSONFormat = function (originData) {
    // this.data = origin_data ? origin_data :
    // JSON && JSON.parse ? JSON.parse(origin_data) : eval('(' + origin_data + ')');
    this.data = JSON.parse(originData)
  }

  _JSONFormat.prototype = {
    constructor: JSONFormat,
    toString: function () {
      return format(this.data, 1)
    }
  }

  return _JSONFormat
})()

window.hide = function (obj) {
  let dataType = obj.parentNode.getAttribute('data-type')
  let dataSize = obj.parentNode.getAttribute('data-size')
  obj.parentNode.setAttribute('data-inner', obj.parentNode.innerHTML)
  if (dataType === 'array') {
    obj.parentNode.innerHTML = '<i  style="cursor:pointer;" class="fa fa-plus-square-o" onclick="show(this)"></i>Array[<span class="json_number">' + dataSize + '</span>]'
  } else {
    obj.parentNode.innerHTML = '<i  style="cursor:pointer;" class="fa fa-plus-square-o" onclick="show(this)"></i>Object{...}'
  }
}

window.show = function (obj) {
  obj.parentNode.innerHTML = obj.parentNode.getAttribute('data-inner')
}

export {JSONFormat}
